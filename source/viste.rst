Viste
=====

Le viste costituiscono la parte più corposa del database: facilitando la creazione di summary tramite QGIS, sono usate estensivamente.

La vista principale è una vista materializzata, che si preoccupa di recuperare ed assemblare informazioni da:

    - iNaturalist, per il tramite della tabella :code:`inat` (popolata dalla funzione :code:`update_inat_obs()` che a sua volta chiama :code:`get_inat_obs_record(...)`: si vedano i capitoli :ref:`funzioni_r` e :ref:`funzioni_psql` per dettagli);
    - ornitho.it, recuperando informazioni tramite un *foreign data wrapper* da una tabella posta sul server;
    - osservazioni occasionali, con una logica simile a quella di cui sopra;
    - osservazioni provenienti dal database principale.

.. note:: Per ragioni di sicurezza (le impostazioni contengono nomi utenti e password di amministrazione), non vegono riportate le definizioni dei foreign servers. Per una guida alla loro creazione, si veda `foreign_servers`_. 

.. _`foreign_servers`: https://www.postgresql.org/docs/9.4/sql-createserver.html

Foreign Data Wrappers
---------------------

I tre *wrapper* implementati servono al recupero delle informazioni su tabelle fisiche scritte sul disco e da un altro database. 

Per le osservazioni da ornitho.it:

.. code-block:: psql

    CREATE FOREIGN TABLE atlas.aartn_ornithobs
       (id_orn integer ,
        id_spec integer ,
        spec text ,
        id_obs integer ,
        spec_sci text ,
        "grouping" text ,
        fam text ,
        ord text ,
        date date ,
        day integer ,
        month integer ,
        year integer ,
        yearday integer ,
        pentade integer ,
        decade integer ,
        yearweek integer ,
        time_1 real ,
        time_2 integer ,
        time_3 integer ,
        time_4 real ,
        time_5 integer ,
        time_6 integer ,
        complete integer ,
        "time" time without time zone ,
        id_loc integer ,
        loc text ,
        comune text ,
        prov text ,
        country text ,
        lat text ,
        lon text ,
        lat_1 text ,
        lon_2 text ,
        utm integer ,
        utm_x integer ,
        utm_y integer ,
        loc_type text ,
        altitude integer ,
        stima text ,
        count integer ,
        details text ,
        obs text ,
        protect text ,
        verify text ,
        behaviour text ,
        name text ,
        surname text ,
        search integer ,
        surname_1 text ,
        secondhand text ,
        note text ,
        note_reserved text ,
        comment text ,
        date_insertion text ,
        date_last text ,
        prot text ,
        mortality text )
       SERVER aartn_singleobs
       OPTIONS (filename '/home/webgisvm/webgis/src/aartn/dbfiles/ornitho/all.csv', format 'csv', delimiter ';', header 'true');

Per le osservazioni occasionali:

.. code-block:: psql

    CREATE FOREIGN TABLE atlas.aartn_singleobs
       (id integer ,
        specie text ,
        class text ,
        data date ,
        y real ,
        x real ,
        note text ,
        obs text )
       SERVER aartn_singleobs
       OPTIONS (filename '/home/webgisvm/webgis/src/aartn/dbfiles/single/singleobs.csv', format 'csv', delimiter ';', header 'true');

Per le osservaioni da database collegato (recupero solo le informazioni strettamente necessarie):

.. code-block:: psql

    CREATE FOREIGN TABLE atlas.bioentry
       (taxon_fk uuid NOT NULL,
        bioentry_id integer NOT NULL,
        geometry geometry(6612988) NOT NULL,
        eventdate date ,
        individualcountbottom integer )
       SERVER biosql
       OPTIONS (schema_name 'public', table_name 'bioentry');

Ed infine, la tabella delle tassonomie per completare le info relative alle osservazioni da database:

.. code-block:: psql

    CREATE FOREIGN TABLE atlas.taxon
       (taxon_pk uuid NOT NULL,
        kingdom text ,
        class text ,
        ordo text ,
        family text ,
        genus text ,
        specie text ,
        vernacularname_it text )
       SERVER biosql
       OPTIONS (schema_name 'public', table_name 'taxon');

Vista AARTN
-----------

È la vista principale dalla quale tutte le altre dipendono a cascata. Svolge anche alcuni prefiltri sui dati.

.. code-block:: psql

    CREATE MATERIALIZED VIEW atlas.aartn AS 
     WITH intersectgeom AS (
             SELECT st_transform(st_buffer(pat.geom, 1000::double precision), 25832) AS boundary
               FROM main.pat
            ), excluded(specie) AS (
             VALUES ('Amphibia sp.'::text), ('Anguis'::text), ('Anguis sp.'::text), ('Anura'::text), ('Bufotes'::text), ('Colubridae'::text), ('Emydidae'::text), ('Lacerta'::text), ('Natrix'::text), ('Pelophylax'::text), ('Rana'::text), ('Ranidae'::text), ('Salamandridae'::text), ('Testudines'::text), ('Testudo'::text), ('Trachemys'::text), ('Vipera'::text)
            ), mainquery AS (
             SELECT DISTINCT taxon.specie,
                taxon.class,
                bioentry.eventdate::text AS eventdate,
                'ZOOLDB'::text AS fromwhichdb,
                st_y(bioentry.geometry)::integer AS y,
                st_x(bioentry.geometry)::integer AS x,
                bioentry.geometry
               FROM atlas.bioentry,
                atlas.taxon
              WHERE bioentry.taxon_fk = taxon.taxon_pk AND (taxon.class = ANY (ARRAY['Amphibia'::text, 'Reptilia'::text]))
            UNION ALL
             SELECT DISTINCT inat.specie,
                inat.class,
                inat.data,
                inat.fromwhichdb,
                inat.y,
                inat.x,
                st_setsrid(st_makepoint(inat.x::double precision, inat.y::double precision), 25832) AS geometry
               FROM atlas.inat
            UNION ALL
             SELECT DISTINCT aartn_ornithobs_clean.specie,
                aartn_ornithobs_clean.class,
                aartn_ornithobs_clean.eventdate::text AS eventdate,
                aartn_ornithobs_clean.fromwhichdb,
                aartn_ornithobs_clean.y,
                aartn_ornithobs_clean.x,
                st_setsrid(st_makepoint(aartn_ornithobs_clean.x, aartn_ornithobs_clean.y), 25832) AS geometry
               FROM atlas.aartn_ornithobs_clean
            UNION ALL
             SELECT DISTINCT aartn_singleobs_clean.specie,
                aartn_singleobs_clean.class,
                aartn_singleobs_clean.data::text AS eventdate,
                'LOCAL'::text AS text,
                aartn_singleobs_clean.y,
                aartn_singleobs_clean.x,
                st_setsrid(st_makepoint(aartn_singleobs_clean.x, aartn_singleobs_clean.y), 25832) AS geometry
               FROM atlas.aartn_singleobs_clean
            )
     SELECT DISTINCT ON (btrim(mainquery.specie), mainquery.class, mainquery.eventdate, mainquery.fromwhichdb, mainquery.y, mainquery.x, mainquery.geometry)
            CASE
                WHEN btrim(mainquery.specie) = 'Pseudepidalea viridis'::text THEN 'Bufotes viridis'::text
                ELSE btrim(mainquery.specie)
            END AS specie,
        mainquery.class,
        mainquery.eventdate::date AS eventdate,
        mainquery.fromwhichdb,
        mainquery.y::integer AS y,
        mainquery.x::integer AS x,
        mainquery.geometry,
        row_number() OVER () AS id
       FROM mainquery,
        excluded e,
        intersectgeom
      WHERE NOT (EXISTS ( SELECT 1
               FROM excluded e_1
              WHERE mainquery.specie = e_1.specie)) AND mainquery.eventdate <> ''::text AND st_intersects(intersectgeom.boundary, mainquery.geometry)
    WITH DATA;

Un altro paio di viste materializzate restituiscono le osservazioni dalle tabelle collegate, ma ripulite.

Per ornitho.it:

.. code-block:: psql

    CREATE MATERIALIZED VIEW atlas.aartn_ornithobs_clean AS 
     SELECT aartn_ornithobs.id_orn AS id,
        replace(replace(replace(aartn_ornithobs.spec_sci, 'Bufotes balearicus / siculus / viridis'::text, 'Bufotes viridis'::text), 'Pelophylax kurtmuelleri / ridibundus'::text, 'Pelophylax ridibundus'::text), 'Lacerta viridis / bilineata'::text, 'Lacerta bilineata'::text) AS specie,
            CASE aartn_ornithobs.grouping
                WHEN 'Anfibi'::text THEN 'Amphibia'::text
                WHEN 'Rettili'::text THEN 'Reptilia'::text
                ELSE ''::text
            END AS class,
        aartn_ornithobs.date AS eventdate,
        st_x(st_transform(st_setsrid(st_makepoint(replace(aartn_ornithobs.lon, ','::text, '.'::text)::double precision, replace(aartn_ornithobs.lat, ','::text, '.'::text)::double precision), 4326), 25832)) AS x,
        st_y(st_transform(st_setsrid(st_makepoint(replace(aartn_ornithobs.lon, ','::text, '.'::text)::double precision, replace(aartn_ornithobs.lat, ','::text, '.'::text)::double precision), 4326), 25832)) AS y,
        st_transform(st_setsrid(st_makepoint(replace(aartn_ornithobs.lon, ','::text, '.'::text)::double precision, replace(aartn_ornithobs.lat, ','::text, '.'::text)::double precision), 4326), 25832) AS geometry,
        'ORNITHO'::text AS fromwhichdb
       FROM atlas.aartn_ornithobs
    WITH DATA;

Per le osservazioni occasionali:

.. code-block:: psql

    CREATE MATERIALIZED VIEW atlas.aartn_singleobs_clean AS 
     SELECT aartn_singleobs.id,
        aartn_singleobs.specie,
        aartn_singleobs.class,
        aartn_singleobs.data,
        st_x(st_transform(st_setsrid(st_makepoint(aartn_singleobs.x::double precision, aartn_singleobs.y::double precision), 4326), 25832)) AS x,
        st_y(st_transform(st_setsrid(st_makepoint(aartn_singleobs.x::double precision, aartn_singleobs.y::double precision), 4326), 25832)) AS y
       FROM atlas.aartn_singleobs
    WITH DATA;

Viste riassuntive
-----------------

.. note:: Le viste sono elencate in ordine di dipendenza.

Numero di specie per cella dopo il 2001:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_species_in_grid_post_2001 AS 
    (
    SELECT grid.gridid,
        grid.geom,
        count(DISTINCT aartn.specie) AS n_spec,
        array_agg(DISTINCT aartn.specie) AS spec
        FROM atlas.grid,
        atlas.aartn
    WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate > '2002-01-01'::date
    GROUP BY grid.gridid, grid.geom
    UNION
    SELECT grid.gridid,
        grid.geom,
        0 AS n_spec,
        ARRAY[''::text] AS spec
        FROM atlas.grid
    ) EXCEPT
    SELECT DISTINCT grid.gridid,
        grid.geom,
        0 AS n_spec,
        ARRAY[''::text] AS spec
    FROM atlas.grid,
    atlas.aartn
    WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate > '2002-01-01'::date;

Numero di specie per ogni cella:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_species_in_grid AS 
    (
             SELECT grid.gridid,
                grid.geom,
                count(DISTINCT aartn.specie) AS n_spec,
                array_agg(DISTINCT aartn.specie) AS spec
               FROM atlas.grid,
                atlas.aartn
              WHERE st_intersects(aartn.geometry, grid.geom)
              GROUP BY grid.gridid, grid.geom
            UNION
             SELECT grid.gridid,
                grid.geom,
                0 AS n_spec,
                ARRAY[''::text] AS spec
               FROM atlas.grid
    ) EXCEPT
     SELECT DISTINCT grid.gridid,
        grid.geom,
        0 AS n_spec,
        ARRAY[''::text] AS spec
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(grid.geom, aartn.geometry);

Numero di specie post-2017 (intese come "nuove specie in ogni griglia, quindi non avvistate prima del 2017") in ogni cella della griglia:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_new_specs_grid AS 
     SELECT DISTINCT aartn_species_in_grid_post_2001.gridid,
        aartn_species_in_grid_post_2001.geom,
        ARRAY( SELECT unnest(aartn_species_in_grid_post_2001.spec) AS unnest
            EXCEPT
             SELECT unnest(aartn_species_in_grid.spec) AS unnest) AS new_specs
       FROM atlas.aartn_species_in_grid_post_2001,
        atlas.aartn_species_in_grid
      WHERE aartn_species_in_grid_post_2001.gridid = aartn_species_in_grid.gridid AND array_length(ARRAY( SELECT unnest(aartn_species_in_grid_post_2001.spec) AS unnest
            EXCEPT
             SELECT unnest(aartn_species_in_grid.spec) AS unnest), 1) > 0 AND (ARRAY( SELECT unnest(aartn_species_in_grid_post_2001.spec) AS unnest
            EXCEPT
             SELECT unnest(aartn_species_in_grid.spec) AS unnest)) <> ARRAY[''::text];

Numero di osservazioni per ogni cella:

.. code-block:: psql 

    CREATE OR REPLACE VIEW atlas.aartn_obs_in_grid AS 
    (
             SELECT grid.gridid,
                grid.geom,
                count(aartn.specie) AS n_obs
               FROM atlas.grid,
                atlas.aartn
              WHERE st_intersects(aartn.geometry, grid.geom)
              GROUP BY grid.gridid, grid.geom
            UNION
             SELECT grid.gridid,
                grid.geom,
                0 AS n_obs
               FROM atlas.grid
    ) EXCEPT
     SELECT grid.gridid,
        grid.geom,
        0 AS n_obs
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom)
      GROUP BY grid.gridid, grid.geom;

Numero di osservazioni per ogni cella dopo il 2001:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_obs_in_grid_post_2001 AS 
    (
             SELECT grid.gridid,
                grid.geom,
                count(aartn.specie) AS n_obs
               FROM atlas.grid,
                atlas.aartn
              WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate > '2002-01-01'::date
              GROUP BY grid.gridid, grid.geom
            UNION
             SELECT grid.gridid,
                grid.geom,
                0 AS n_obs
               FROM atlas.grid
    ) EXCEPT
     SELECT grid.gridid,
        grid.geom,
        0 AS n_obs
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate > '2002-01-01'::date
      GROUP BY grid.gridid, grid.geom;

Osservazioni per ogni cella dopo il 2017:

.. code-block:: psql 

    CREATE OR REPLACE VIEW atlas.aartn_obs_in_grid_post_2017 AS 
     SELECT row_number() OVER () AS id,
        grid.gridid,
        grid.geom,
        aartn.class,
        count(aartn.specie) AS n_obs
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate > '2017-01-01'::date
      GROUP BY grid.gridid, grid.geom, aartn.class;

Osservazioni per ogni cella prima del 2017:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_obs_in_grid_pre_2017 AS 
     SELECT row_number() OVER () AS id,
        grid.gridid,
        grid.geom,
        aartn.class,
        count(aartn.specie) AS n_obs
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate < '2017-01-01'::date
      GROUP BY grid.gridid, grid.geom, aartn.class;

Osservazioni per ogni cella prima del 2017, per tutte le classi:

.. code-block:: psql 

    CREATE OR REPLACE VIEW atlas.aartn_obs_in_grid_pre_2017_allclass AS 
     SELECT row_number() OVER () AS id,
        grid.gridid,
        grid.geom,
        count(aartn.specie) AS n_obs
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate < '2017-01-01'::date
      GROUP BY grid.gridid, grid.geom;

Specie per ogni cella dopo il 2017:

.. code-block:: psql 

    CREATE OR REPLACE VIEW atlas.aartn_species_in_grid_post_2017 AS 
     SELECT row_number() OVER () AS id,
        grid.gridid,
        grid.geom,
        aartn.class,
        count(DISTINCT aartn.specie) AS n_spec,
        array_agg(DISTINCT aartn.specie) AS spec
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate > '2017-01-01'::date
      GROUP BY grid.gridid, grid.geom, aartn.class;

Specie per ogni cella prima del 2017:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_species_in_grid_pre_2017 AS 
     SELECT row_number() OVER () AS id,
        grid.gridid,
        grid.geom,
        aartn.class,
        count(DISTINCT aartn.specie) AS n_spec,
        array_agg(DISTINCT aartn.specie) AS spec
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom) AND aartn.eventdate < '2017-01-01'::date
      GROUP BY grid.gridid, grid.geom, aartn.class;

Specie nuove (mai osservate prima del 2017) per ogni cella:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_reallynew_species AS 
     WITH one AS (
             SELECT que.gridid,
                array_agg(que.specie) AS specie
               FROM ( SELECT a.gridid,
                        unnest(a.spec) AS specie
                       FROM atlas.aartn_species_in_grid_post_2017 a) que
              GROUP BY que.gridid
            )
     SELECT grid.gridid,
        grid.geom,
        array_to_string(array_agg(t.spec), ','::text, '*'::text) AS array_agg,
        count(t.spec) AS n_spec
       FROM ( SELECT one.gridid,
                unnest(one.specie) AS spec
               FROM one
            EXCEPT
             SELECT aartn_species_in_grid_pre_2017.gridid,
                unnest(aartn_species_in_grid_pre_2017.spec) AS spec
               FROM atlas.aartn_species_in_grid_pre_2017) t,
        atlas.grid
      WHERE grid.gridid = t.gridid
      GROUP BY grid.gridid, grid.geom
      ORDER BY grid.gridid;

Numero di specie per ogni cella:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_species_in_grid_all AS 
     SELECT row_number() OVER () AS id,
        grid.gridid,
        grid.geom,
        aartn.class,
        count(DISTINCT aartn.specie) AS n_spec,
        array_agg(DISTINCT aartn.specie) AS spec
       FROM atlas.grid,
        atlas.aartn
      WHERE st_intersects(aartn.geometry, grid.geom) 
      GROUP BY grid.gridid, grid.geom, aartn.class;


Viste di supporto
-----------------

Queste viste servono per tener traccia del lavoro svolto specificatamente all'interno del progetto atlante.

Per verificare quali celle sono state riempite di nuovi dati, dal 2001 in poi:

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.aartn_updated_cells AS 
     SELECT DISTINCT grid.id,
        grid.geom
       FROM atlas.grid,
        atlas.aartn
      WHERE aartn.eventdate IS NOT NULL AND aartn.eventdate > '2002-01-01'::date AND st_intersects(grid.geom, aartn.geometry);

Per dare una idea delle celle ancora da visitare e di quelle già visitate (rispetto alla tabella :code:`visita_celle` dove l'informazione viene effettivamente immagazzinata. Si veda :ref:`tabelle` per dettagli):

.. code-block:: psql

    CREATE OR REPLACE VIEW atlas.riassunto_visita_celle AS 
     SELECT grid.gridid,
        grid.geom,
        visita_celle.mese,
        visita_celle.visitata_1,
        visita_celle.visitata_2,
            CASE
                WHEN date_part('month'::text, 'now'::text::date) > visita_celle.mese::double precision AND visita_celle.visitata_1 = false THEN 'CRITICA'::text
                WHEN date_part('month'::text, 'now'::text::date) = visita_celle.mese::double precision AND visita_celle.visitata_1 = false THEN 'DA_VISITARE_ORA'::text
                WHEN date_part('month'::text, 'now'::text::date) >= visita_celle.mese::double precision AND visita_celle.visitata_1 = true AND visita_celle.visitata_2 = false THEN 'SECONDA_VISITA'::text
                WHEN date_part('month'::text, 'now'::text::date) < visita_celle.mese::double precision THEN 'FUTURA'::text
                ELSE 'OK'::text
            END AS status
       FROM atlas.grid
         JOIN atlas.visita_celle ON visita_celle.gridid = grid.gridid;

