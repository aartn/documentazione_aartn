Funzioni R-plr
========

Installazione
-------------

.. note:: Il pacchetto :code:`plr` deve essere installato all'interno del database PostgreSQL, con le necessarie dipendenze configurate da dentro R.

Sotto Ubuntu 18.04 il necessario si installa con i seguenti comandi:

Prima si installa R e l'estensione necessaria per psql:

.. code-block:: bash

	sudo apt install r-base-core postgresql-*-plr
	sudo R # l'installazione system-wide dà in genere meno problemi di configurazione

Quindi, da R si installa il pacchetto necessario :code:`rinat`:

.. code-block:: R

	install.packages('rinat',dependencies=TRUE)

Ed infine, connettendosi al database...

.. code-block:: bash

	psql database

Si installa l'estensione.

.. code-block:: psql

	CREATE EXTENSION plr;

Definizioni
-------------

L'unica funzione interessante all'interno del database è quella che permette il recupero delle informazioni di una certa specie per il tramite:
* della API DI `iNaturalist`_
* del pacchetto `rinat`_ (parte dei pacchetti dedicati al recupero dei dati di presenza di specie -`SPOCC`_ della fondazione `ROpenSci`_-)

.. _iNaturalist: https://www.inaturalist.org
.. _rinat: https://cran.r-project.org/web/packages/rinat/index.html
.. _SPOCC: https://github.com/ropensci/spocc
.. _ROpenSci: https://ropensci.org

.. code-block:: psql

	SELECT get_inat_obs_record('Amphibia',st_astext(pat.geom),1) FROM main.pat

La vera funzione è :code:`get_inat_obs_record`, che accetta in input:
	- il taxon da ricercare,
	- la geometria rispetto alla quale filtrare TODO: far accettare qualsiasi SRID
	- il numero di record che si desidera vedersi restituire

.. code-block:: psql

	CREATE OR REPLACE FUNCTION get_inat_obs_record(
	    search_key text,
	    geometry text DEFAULT NULL::text,
	    maxobs integer DEFAULT 100)
	  RETURNS SETOF atlas.inat AS
	$BODY$
		-- corpo R della funzione
	$BODY$
	  LANGUAGE plr VOLATILE;

.. code-block:: R

		library(c('rgeos','rinat','rgdal','sp'))

		# Sistema di riferimento dei dati INAT
		wgs84<-"+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
		# Sistema di riferimento di destinazione
		utm32n<-"+proj=utm +zone=32 +ellps=GRS80 +units=m +no_defs"

		# Lettura della geometria di filtro, riproiezione ed estrazione dei boundary
		shp <- readWKT(geometry,p4s=utm32n)
		shpwgs84<-spTransform(shp,wgs84)
		bounds<-bbox(shpwgs84)
		bounds<-c(bounds[2,1],bounds[1,1],bounds[2,2],bounds[1,2])

		# Recupero delle osservazioni da INAT
		obss <- get_inat_obs(taxon_name = search_key, bounds = bounds, geo = TRUE, maxresult=maxobs)

		# Creazoone del dataframe geografico + riproiezione dello stesso
		coordinates(obss)<-~longitude+latitude
		proj4string(obss)<-CRS(wgs84)
		obss<-spTransform(obss,CRS=utm32n)

		# Pulizia ed export
		obss<-obss[,c("id","scientific_name","datetime")]
		obss$from<-"INAT"
		obss<-(as.data.frame(obss))
		colnames(obss)<-c("id","specie","evtdate","fromwhichdb","x","y")
		obss$x<-round(obss$x)
		obss$y<-round(obss$y)
		obss$class <- search_key
		
		return(obss)

Utilizzo
-------------

La funzione è fatta per restituire un set di righe consistente con la tabella :code:`inat` :

.. code-block:: psql

	CREATE TABLE atlas.inat
	(
	  id integer,
	  specie text,
	  data text,
	  fromwhichdb text,
	  x integer,
	  y integer,
	  class text
	)

Di conseguenza, per essere usata all'interno di altre query che operano su tabelle, vanno specificate quali sono le colonne che si desidera vedersi ritornare:

.. code-block:: psql

	SELECT t.id,specie,data,fromwhichdb,x,y
	FROM main.pat,get_inat_obs_record('Amphibia',st_astext(pat.geom),1) AS t

All'interno del database, la funzione è chiamata regolarmente da un trigger che va a popolare la tabella :code:`inat`, a sua volta recuperata da una vista materializzata per l'assemblamento dei dati di anfibi e rettili da varie fonti (si veda la sezione :ref:`funzioni_psql` per dettagli)