Funzioni psql
=============

Qua sono presenti solo dei smeplici trigger che regolarmente popolano la tabella :code:`inat` per  il tramite di :code:`get_inat_obs_record()` (si feda :ref:`funzioni_r` per dettagli). C'è inoltre una utility per la manutenzione delle viste materializzate.

Per il popolamento della tabella :code:`inat`, si usa semplicemente:

.. code-block:: psql

	-- Rettili
	INSERT INTO inat
	SELECT t.id,specie,data,fromwhichdb,x,y FROM main.pat,get_inat_obs_record('Amphibia',st_astext(pat.geom),100000)AS t;
	-- Anfibi
	INSERT INTO inat
	SELECT t.id,specie,data,fromwhichdb,x,y FROM main.pat,get_inat_obs_record('Reptilia',st_astext(pat.geom),100000)AS t;


La funzione per effettuare l'update, :code:`update_inat_aartn()`, è un semplice *wrapper* attorno ai due statement di cui sopra:

.. code-block:: psql

	CREATE OR REPLACE FUNCTION update_inat_aartn()
	  RETURNS void AS
	$BODY$
	    BEGIN
		TRUNCATE atlas.inat;
		INSERT INTO atlas.inat
		SELECT t.id,specie,data,fromwhichdb,x,y,class FROM main.pat,get_inat_obs_record('Amphibia'::text, st_astext(pat.geom), 1000000) AS t;
		INSERT INTO atlas.inat
		SELECT t.id,specie,data,fromwhichdb,x,y,class FROM main.pat,get_inat_obs_record('Reptilia'::text, st_astext(pat.geom), 1000000) AS t;
	    END;
	$BODY$
	  LANGUAGE plpgsql VOLATILE;
