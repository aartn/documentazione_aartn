Benvenui alla pagina di documentazione per il database AARTN!
================================================================

La presente documentazione serve per raccapezzarsi nella relativa
complessità del database utilizzato per generare i report dell'Atlante
degli Anfibi e dei Rettili del Trentino.

.. toctree::
	:maxdepth: 2
	:caption: Contenuti:

	funzioni_r
	funzioni_psql
	tabelle
	viste
	integrazioni_qgis

.. note:: Si presume una conoscenza almeno di base delle funzioni di *R* e di *PostgreSQL*, comunque non indispensabile per la comprensione generale