Tabelle
=======

Le tabelle disponibili all'interno del database sono tutte piuttosto banali. Solo un paio meritano attenzione.

La tabella utile ad immagazzinare le osservazioni provenienti da iNaturalist:

.. code-block:: psql

	CREATE TABLE atlas.inat
	(
	  id integer,
	  specie text,
	  data text,
	  fromwhichdb text,
	  x integer,
	  y integer,
	  class text
	)

La tabella della griglia, che suddivide il territorio trentino in celle da 5x5 km rispetto alle quali
creare i vari summary:

.. code-block:: psql

	CREATE TABLE atlas.grid
	(
	  id serial NOT NULL,
	  geom geometry(MultiPolygon,25832),
	  gridid integer,
	  CONSTRAINT grid_pkey PRIMARY KEY (id)
	)

Quella che contiene la calendarizzazione prevista delle varie celle:

.. code-block:: psql

	CREATE TABLE atlas.visita_celle
	(
	  gridid integer NOT NULL,
	  visitata_1 boolean,
	  visitata_2 boolean,
	  mese smallint,
	  CONSTRAINT visita_celle_gridid_pk PRIMARY KEY (gridid)
	)

Ed infine, la tabella che dettaglia come il database -ed in particolare la vista materializzata (rif. :ref:`viste`) :code:`aartn`- si deve comportare di fronte a nomenclature di specie imperfette.

In particolare, la tabella per il tramite della colonna :code:`todo`:

	* mantiene il nome originale, se questo va bene
	* modifica il nome originale, se ha senso farlo (*Bufo viridis* > *Bufotes viridis*, *Anguis sp.* > *Anguis veronensis*. Altro esempio *Lacerta viridis*, il Ramarro orientale, specie che considera Fusine (FVG) come sua unica porzione di areale italiana: quindi nessun ramarro visto in Trentino è orientale. Di conseguenza *Lacerta viridis* > *Lacerta bilineata* )
	* elimina il nome, e quindi, anche l'osservazione collegata a quel nome. 

.. code-block:: psql

	CREATE TABLE atlas.species_correspondence
	(
	  id integer NOT NULL,
	  specie text,
	  status text,
	  todo text,
	  CONSTRAINT species_correspondence_pk PRIMARY KEY (id)
	)