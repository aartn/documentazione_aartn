Integrazione con QGIS
=====================

Tutto il progetto arriva completo di un progetto QGIS altamente organizzato, utile alla consultazione (e in un caso, anche all'editing diretto) e all'export delle varie viste (:ref:`viste`) che compongono l'Atlante. 

.. image:: _static/img/qgis_aartn.png
   :height: 350px
   :width: 700 px
   :alt: Immagine del progetto QGIS dell'AARTN
   :align: center

.. note:: Il progetto necessita di una versione di QGIS 3.0 o superiore

Il progetto è liberamente scaricabile (`link_proj`_) ed è preimpostato con un account di sola lettura al quale si accede con le seguenti credenziali:

	* nome utente: **aartn_readonly**
	* password: **aartn**

Collegandosi il progetto ad un database visibile solo dalla rete interna del MUSE - Museo delle Scienze, lo stesso va quindi aperto con il PC connesso ad una qualche rete -via cavo o wireless- del MUSE, oppure con una VPN alla rete MUSE attiva.

.. _link_proj: _static/files/aartn_qgis.qgz
