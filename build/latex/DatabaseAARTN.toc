\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{Contenuti:}{1}{chapter*.1}
\contentsline {chapter}{\numberline {1}Funzioni R-plr}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Installazione}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Definizioni}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Utilizzo}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Funzioni psql}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Tabelle}{6}{chapter.3}
\contentsline {chapter}{\numberline {4}Viste}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Foreign Data Wrappers}{8}{section.4.1}
\contentsline {section}{\numberline {4.2}Vista AARTN}{10}{section.4.2}
\contentsline {section}{\numberline {4.3}Viste riassuntive}{12}{section.4.3}
\contentsline {section}{\numberline {4.4}Viste di supporto}{16}{section.4.4}
\contentsline {chapter}{\numberline {5}Integrazione con QGIS}{17}{chapter.5}
